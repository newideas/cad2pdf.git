﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using Autodesk.AutoCAD;
//using Autodesk.AutoCAD.Interop;
//using Autodesk.AutoCAD.Interop.Common;
//using Autodesk.AutoCAD.Runtime;
//using Autodesk.AutoCAD.ApplicationServices;
//using Autodesk.AutoCAD.DatabaseServices;
//using Autodesk.AutoCAD.Geometry;
//using Autodesk.AutoCAD.EditorInput;
//using Autodesk.AutoCAD.Windows;
using AutoCAD;
using System.IO;
using Spire.Pdf;
using System.Threading;

namespace cad2pdf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            richTextBox1.AllowDrop = true;
            richTextBox1.DragDrop += RichTextBox1_DragDrop;
            richTextBox1.DragEnter += RichTextBox1_DragEnter;

            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                checkedListBox1.SetItemChecked(i, true);
            }
            richTextBox_pdfs.AllowDrop = true;
            richTextBox_pdfs.DragDrop += RichTextBox_pdfs_DragDrop;
            richTextBox_pdfs.DragEnter += RichTextBox_pdfs_DragEnter;


        }

        private void RichTextBox_pdfs_DragEnter(object sender, DragEventArgs e)
        {
            //throw new NotImplementedException();
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Link;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void RichTextBox_pdfs_DragDrop(object sender, DragEventArgs e)
        {
            richTextBox_pdfs.Text = richTextBox_pdfs.Text.Replace("拖放要合并的pdf文件到此", "");
            richTextBox_pdfs.Text = richTextBox_pdfs.Text.Replace("根据排列顺序合并", "");
            richTextBox_pdfs.Text = richTextBox_pdfs.Text.TrimStart('\n').TrimStart();
            foreach (var item in ((System.Array)e.Data.GetData(DataFormats.FileDrop)))
            {
                richTextBox_pdfs.Text += item.ToString() + '\n';
            }
        }

        private void RichTextBox1_DragEnter(object sender, DragEventArgs e)
        {
            //throw new NotImplementedException();
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Link;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void RichTextBox1_DragDrop(object sender, DragEventArgs e)
        {
            //throw new NotImplementedException();
            foreach (var item in ((System.Array)e.Data.GetData(DataFormats.FileDrop)))
            {
                richTextBox1.Text = item.ToString();
            }
        }

        double calcDiagonal(object point1, object point2)
        {
            double length = Math.Abs(((double[])point1)[0] - ((double[])point2)[0]);
            double height = Math.Abs(((double[])point1)[1] - ((double[])point2)[1]);

            double result = Math.Sqrt(length * length + height * height);//对角线
            return result;
        }

        private void button_start_Click(object sender, EventArgs e)
        {

            new Thread(delegate ()
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    button_start.Enabled = false;
                }));



                int length = (int)double.Parse(textBox_length.Text);
                int height = (int)double.Parse(textBox_height.Text);
                double minDiagonal = Math.Sqrt(length * length + height * height);//计算最小对角线

                string filePath = "";

                this.Invoke(new MethodInvoker(delegate ()
                {
                    filePath = richTextBox1.Text;
                }));
                AcadDocument AcdDoc = new AcadDocument();
                AcadDocument doc = null;

                doc = AcdDoc.Application.Documents.Open(filePath, true, null);   //以只读方式打开该文件     
                                                                                 //doc.SetVariable("sdi", 0);
                                                                                 //doc.SetVariable("Filedia", 0);
                                                                                 //doc.SetVariable("RASTERPREVIEW", 1);//控制是否将缩略图预览图像随图形一起创建和保存。
                doc.SetVariable("BACKGROUNDPLOT", 0);//前台打印
                doc.Application.ZoomExtents();
                doc.ActiveLayout.ConfigName = "DWG To PDF.pc3";//使用的打印机设置名称 页面设置名称

                //object LowerLeft;//左下坐标，即xy最小值
                //object UpperRight;//右上坐标，即xy最大值
                //doc.ActiveLayout.GetWindowToPlot(out LowerLeft, out UpperRight);

                List<object[]> ls_rec = new List<object[]>();
                for (int i = 0; i < doc.ModelSpace.Count; i++)
                {

                    if (i % 97 == 1)
                    {
                        this.Invoke(new MethodInvoker(delegate ()
                        {
                            button_start.Text = "正在查找对象：" + i + "/" + doc.ModelSpace.Count;
                        }));
                    }
                    //AcadEntity cadObject = doc.ModelSpace.Item(i);
                    dynamic cadObject = doc.ModelSpace.Item(i);

                    Console.WriteLine(cadObject.EntityName);
                    //AcDbBlockReference
                    //AcDbMText
                    //AcDbLine
                    //AcDbPolyline
                    //AcDbText
                    //AcDbOle2Frame
                    //AcDbCircle

                    if (checkedListBox1.GetItemChecked(1) && cadObject.EntityName == "AcDbPolyline")//为矩形类型的才判断
                    {
                        dynamic acadMLine = cadObject;
                        double[] coordinates = acadMLine.Coordinates;
                        //AcadMLine acadMLine = (AutoCAD.AcadMLine)cadObject;
                        //object double3_min, double3_max;
                        //acadMLine.GetBoundingBox(out double3_min, out double3_max);
                        object LowerLeft = new double[2];//左下坐标，即xy最小值
                        object UpperRight = new double[2];//右上坐标，即xy最大值
                        ((double[])LowerLeft)[0] = coordinates.Where((r, index) => index % 2 == 0).Min();
                        ((double[])LowerLeft)[1] = coordinates.Where((r, index) => index % 2 == 1).Min();
                        ((double[])UpperRight)[0] = coordinates.Where((r, index) => index % 2 == 0).Max();
                        ((double[])UpperRight)[1] = coordinates.Where((r, index) => index % 2 == 1).Max();

                        if (calcDiagonal(LowerLeft, UpperRight) > minDiagonal)
                        {
                            ls_rec.Add(new object[] { LowerLeft, UpperRight });
                        }

                    }
                    if (checkedListBox1.GetItemChecked(0) && cadObject.EntityName == "AcDbBlockReference")//为block类型的才判断
                    {
                        AcadBlockReference acadBlock = (AcadBlockReference)cadObject;
                        dynamic block = acadBlock.Explode();
                        if (((object[])(block)).Length > 0)
                        {
                            object double3_min, double3_max;
                            acadBlock.GetBoundingBox(out double3_min, out double3_max);
                            object LowerLeft = new double[2];//左下坐标，即xy最小值
                            object UpperRight = new double[2];//右上坐标，即xy最大值
                            ((double[])LowerLeft)[0] = ((double[])double3_min)[0];
                            ((double[])LowerLeft)[1] = ((double[])double3_min)[1];
                            ((double[])UpperRight)[0] = ((double[])double3_max)[0];
                            ((double[])UpperRight)[1] = ((double[])double3_max)[1];

                            if (calcDiagonal(LowerLeft, UpperRight) > minDiagonal)
                            {
                                ls_rec.Add(new object[] { LowerLeft, UpperRight });
                            }
                        }
                    }
                }

                //ls_rec去除重复值
                for (int i = 0; i < ls_rec.Count; i++)  //外循环是循环的次数
                {
                    for (int j = ls_rec.Count - 1; j > i; j--)  //内循环是 外循环一次比较的次数
                    {
                        object LowerLeft_i = new double[2];//左下坐标，即xy最小值
                        object UpperRight_i = new double[2];//右上坐标，即xy最大值

                        object LowerLeft_j = new double[2];//左下坐标，即xy最小值
                        object UpperRight_j = new double[2];//右上坐标，即xy最大值

                        LowerLeft_i = ((object[])(ls_rec[i]))[0];
                        UpperRight_i = ((object[])(ls_rec[i]))[1];

                        LowerLeft_j = ((object[])(ls_rec[j]))[0];
                        UpperRight_j = ((object[])(ls_rec[j]))[1];
                        if (((double[])LowerLeft_i)[0] == ((double[])LowerLeft_j)[0]
                        && ((double[])LowerLeft_i)[1] == ((double[])LowerLeft_j)[1]
                        && ((double[])UpperRight_i)[0] == ((double[])UpperRight_j)[0]
                        && ((double[])UpperRight_i)[1] == ((double[])UpperRight_j)[1])
                        {
                            ls_rec.RemoveAt(j);
                        }

                    }
                }
                //排序
                //List<object[]> ls_newRecSort = new List<object[]>();                
                //while (ls_rec.Count > 0)
                //{
                //    double yy = ls_rec.Max(r => ((double[])((object[])r)[0])[1]);//y
                //    double xx = ls_rec.Where(r => ((double[])((object[])r)[0])[1] == yy).Min(r => ((double[])((object[])r)[0])[0]);

                //    //double xx = ls_rec.Min(r => ((double[])((object[])r)[0])[0]);//x
                //    //double yy = ls_rec.Where(r => ((double[])((object[])r)[0])[0] == xx).Max(r => ((double[])((object[])r)[0])[1]);
                //    //double yy = ls_rec.Max(r => ((double[])((object[])r)[0])[1]);//y
           
                //    object minLowerLeft = new double[2];//左下坐标，即xy最小值
                //    for (int i = 0; i < ls_rec.Count; i++)  //外循环是循环的次数
                //    {
                //        object LowerLeft_i = new double[2];//左下坐标，即xy最小值
                //        LowerLeft_i = ((object[])(ls_rec[i]))[0];

                //        if (((double[])LowerLeft_i)[0] == xx
                //        && ((double[])LowerLeft_i)[1] == yy)
                //        {
                //            ls_newRecSort.Add(ls_rec[i]);
                //            ls_rec.RemoveAt(i);
                //        }
                //    }
                //}



                int pdfIndex = 1;
                List<string> ls_pdfs = new List<string>();//生成的分开的pdf文件
                foreach (var item in ls_rec)
                {

                    doc.ActiveLayout.SetWindowToPlot(item[0], item[1]);

                    doc.ActiveLayout.PlotType = AcPlotType.acWindow; //打印模式

                    doc.ActiveLayout.StandardScale = AcPlotScale.acScaleToFit; //标准比例，需要UseStandardScale = true;
                    doc.ActiveLayout.UseStandardScale = true; // 使用标准比例
                    doc.ActiveLayout.CenterPlot = true; // 居中
                                                        //doc.ActiveLayout.PaperUnits = AcPlotPaperUnits.acMillimeters; //单位毫米
                                                        //doc.ActiveLayout.PlotRotation = AcPlotRotation.ac0degrees;

                    doc.Plot.QuietErrorMode = true;//生成存档，避免报错

                    string destPath = Path.Combine(Path.GetDirectoryName(filePath), Path.GetFileNameWithoutExtension(filePath) + pdfIndex++ + ".pdf");
                    ls_pdfs.Add(destPath);
                    doc.Plot.PlotToFile(destPath);//第二个参数为打印机名称
                }

                doc.Close(false);
                AcdDoc.Close(false);


                #region 合并pdf
                if (checkBox_combine.Checked)
                {
                    //获取pdf文件集合
                    string outputFile = Path.Combine(Path.GetDirectoryName(filePath), Path.GetFileNameWithoutExtension(filePath) + ".pdf");

                    //合并输出
                    PdfDocumentBase pdfDoc = PdfDocument.MergeFiles(ls_pdfs.ToArray());

                    pdfDoc.Save(outputFile, FileFormat.PDF);
                }


                this.Invoke(new MethodInvoker(delegate ()
                {
                    button_start.Text = "dooone";
                }));

                #endregion

                //this.Close();

            }).Start();


        }

        private void checkBox_combine_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_combine.Checked)
            {
                this.Size = new Size(this.Size.Width, 300);
            }
            else
            {
                this.Size = new Size(this.Size.Width, this.Height + 200);
            }
        }

        private void button_combine_Click(object sender, EventArgs e)
        {
            button_combine.Enabled = false;
            string text = richTextBox_pdfs.Text.TrimEnd('\n');
            String[] files = text.Split('\n');
            string outputFile = Path.GetDirectoryName(files[0]) + "\\输出.pdf";
            PdfDocumentBase doc = PdfDocument.MergeFiles(files);

            doc.Save(outputFile, FileFormat.PDF);

            button_combine.Text = "dooone";
        }
    }
}
