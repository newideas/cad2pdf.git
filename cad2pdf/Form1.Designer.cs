﻿namespace cad2pdf
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.checkBox_combine = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button_start = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBox_length = new System.Windows.Forms.TextBox();
            this.textBox_height = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBox_pdfs = new System.Windows.Forms.RichTextBox();
            this.button_combine = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "block",
            "rectangle、pline"});
            this.checkedListBox1.Location = new System.Drawing.Point(14, 80);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(255, 52);
            this.checkedListBox1.TabIndex = 0;
            // 
            // checkBox_combine
            // 
            this.checkBox_combine.AutoSize = true;
            this.checkBox_combine.Checked = true;
            this.checkBox_combine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_combine.Location = new System.Drawing.Point(14, 211);
            this.checkBox_combine.Name = "checkBox_combine";
            this.checkBox_combine.Size = new System.Drawing.Size(102, 16);
            this.checkBox_combine.TabIndex = 1;
            this.checkBox_combine.Text = "是否合并总pdf";
            this.checkBox_combine.UseVisualStyleBackColor = true;
            this.checkBox_combine.CheckedChanged += new System.EventHandler(this.checkBox_combine_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "图表框类型勾选";
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(92, 238);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(177, 23);
            this.button_start.TabIndex = 3;
            this.button_start.Text = "start";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(14, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(255, 47);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "拖放dwg到此";
            // 
            // textBox_length
            // 
            this.textBox_length.Location = new System.Drawing.Point(14, 175);
            this.textBox_length.Name = "textBox_length";
            this.textBox_length.Size = new System.Drawing.Size(100, 21);
            this.textBox_length.TabIndex = 5;
            this.textBox_length.Text = "380.0";
            // 
            // textBox_height
            // 
            this.textBox_height.Location = new System.Drawing.Point(169, 175);
            this.textBox_height.Name = "textBox_height";
            this.textBox_height.Size = new System.Drawing.Size(100, 21);
            this.textBox_height.TabIndex = 5;
            this.textBox_height.Text = "250.0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "图表框 长度 高度 （最小值）";
            // 
            // richTextBox_pdfs
            // 
            this.richTextBox_pdfs.Location = new System.Drawing.Point(12, 308);
            this.richTextBox_pdfs.Name = "richTextBox_pdfs";
            this.richTextBox_pdfs.Size = new System.Drawing.Size(257, 119);
            this.richTextBox_pdfs.TabIndex = 6;
            this.richTextBox_pdfs.Text = "拖放要合并的pdf文件到此\n根据排列顺序合并";
            // 
            // button_combine
            // 
            this.button_combine.Location = new System.Drawing.Point(92, 445);
            this.button_combine.Name = "button_combine";
            this.button_combine.Size = new System.Drawing.Size(177, 23);
            this.button_combine.TabIndex = 7;
            this.button_combine.Text = "开始合并添加的pdf";
            this.button_combine.UseVisualStyleBackColor = true;
            this.button_combine.Click += new System.EventHandler(this.button_combine_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.button_combine);
            this.Controls.Add(this.richTextBox_pdfs);
            this.Controls.Add(this.textBox_height);
            this.Controls.Add(this.textBox_length);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox_combine);
            this.Controls.Add(this.checkedListBox1);
            this.Name = "Form1";
            this.Text = "dwg导出pdf工具V1.0.1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.CheckBox checkBox_combine;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBox_length;
        private System.Windows.Forms.TextBox textBox_height;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBox_pdfs;
        private System.Windows.Forms.Button button_combine;
    }
}

